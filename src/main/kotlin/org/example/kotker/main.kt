package org.example.kotker

import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.events.AutoJsonEvents
import org.http4k.events.Event

import org.http4k.events.EventFilters
import org.http4k.events.then
import org.http4k.format.Jackson
import org.http4k.server.Http4kServer
import org.http4k.server.Jetty
import org.http4k.server.asServer
import java.time.Clock
import java.time.Duration
import java.time.temporal.Temporal
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread

fun main() {
    val clock = Clock.systemDefaultZone()

    val events =
            EventFilters.AddTimestamp()
                    .then(EventFilters.AddZipkinTraces())
                    .then(AutoJsonEvents(Jackson))

    val signaller = CountDownLatch(1)
    val callingThread = Thread.currentThread()
    Runtime.getRuntime().addShutdownHook(thread(start = false) {
        events(ShutdownInitiated())
        signaller.countDown()
        callingThread.join()
        events(ShutdownCompleted())
    })

    var jettyServer: Http4kServer? = null
    try {
        val starting = Starting(clock.instant())
        events(starting)

        val app = { request: Request -> Response(OK).body("Hello, ${request.query("name")}!") }

        jettyServer = app.asServer(Jetty(9000)).start()

        val startedTime = clock.instant()
        events(Started(startedTime, Duration.between(starting.time, startedTime).toMillis()))
    } catch (e: Exception) {
        events(StartupFailed(e))
    }

    signaller.await()

    try {
        val stopping = Stopping(clock.instant())
        events(stopping)

        jettyServer?.stop()

        val stoppedTime = clock.instant()
        events(Stopped(stoppedTime, Duration.between(stopping.time, stoppedTime).toMillis()))
    } catch (e: Exception) {
        events(StopFailed(e))
    }
}

data class StopFailed(val e: Exception) : Event
data class StartupFailed(val e: Exception) : Event

data class Starting(val time: Temporal): Event
data class Started(val time: Temporal, val startedMillis: Long): Event
data class Stopping(val time: Temporal): Event
data class Stopped(val time: Temporal, val stoppedMillis: Long): Event
class ShutdownInitiated : Event
class ShutdownCompleted : Event
